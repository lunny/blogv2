---
title: 买了一个新的数码相机 佳能A710 IS
date: '2007-04-21 14:22:49'
description: 
tags: 
categories: ['生活']
---

性能参数：710w象素，6倍光学变焦，1/2.5英寸传感器，2.5英寸液晶屏，光学防抖。

[![A710IS]({{urls.media}}/images/2007/04/a710is.jpg)]({{urls.media}}/images/2007/04/a710is.jpg)

用它拍了几张照片，放一张上来瞧瞧！

[![A710 IS拍的照片]({{urls.media}}/images/2007/04/00001.jpg)]({{urls.media}}/images/2007/04/00001.jpg)
